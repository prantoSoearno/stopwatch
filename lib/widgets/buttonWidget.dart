import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  ButtonWidget({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 25),
      height: MediaQuery.of(context).size.height * 0.15,
      // color: Colors.black45,
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ClipOval(
              child: Material(
                color: Color.fromRGBO(1, 135, 122, 100),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0),
                    side: BorderSide(
                        color: Color.fromRGBO(206, 225, 230, 100),
                        width: 5)), // button color
                child: InkWell(
                  splashColor: Colors.white12, // inkwell color
                  child:
                      SizedBox(width: 56, height: 56, child: Icon(Icons.stop, color: Colors.white,)),
                  onTap: () {},
                  
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: ClipOval(
              child: Material(
                color: Color.fromRGBO(1, 135, 122, 100),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(50.0),
                    side: BorderSide(
                        color: Color.fromRGBO(206, 225, 230, 100),
                        width: 5)), // button color
                child: InkWell(
                  splashColor: Colors.white12, // inkwell color
                  child:
                      SizedBox(width: 70, height: 70, child: Icon(Icons.pause, color: Colors.white,)),
                  onTap: () {},
                ),
              ),
            ),
            ),
            ClipOval(
              child: Material(
                color: Color.fromRGBO(1, 135, 122, 100),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0),
                    side: BorderSide(
                        color: Color.fromRGBO(206, 225, 230, 100),
                        width: 5)), // button color
                child: InkWell(
                  splashColor: Colors.white12, // inkwell color
                  child:
                      SizedBox(width: 56, height: 56, child: Icon(Icons.repeat,color: Colors.white,)),
                  onTap: () {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
