import 'package:flutter/material.dart';

class StopwatchWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.blue,
      height: MediaQuery.of(context).size.height * 0.25,
      child: Column(
        children: [
          Container(
            
            margin: EdgeInsets.only(top: 70),
            height: MediaQuery.of(context).size.height * 0.07,
            width: MediaQuery.of(context).size.width * 0.40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                // color: Colors.black12,
                border: Border(
                    bottom: BorderSide(width: 1.0, color: Colors.white70)
                    )
                    ),
            child: Text(
              'STOPWATCH',
              style: TextStyle(
                fontFamily: 'Raleway',
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 22,
                shadows: <Shadow>[
                Shadow(
        offset: Offset(0.0, .0),
        blurRadius: 5.0,
        color: Colors.black45
      ),]
              ),
            ),
          ),
        ],
      ),
    );
  }
}
