import 'package:flutter/material.dart';
import 'dart:ui';

class TimeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.red,
      margin: EdgeInsets.only(top: 0),
      height: MediaQuery.of(context).size.height * 0.16,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Text(
                    'hours',
                    style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(207, 224, 233, 10)),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.all(17)),
              Column(
                children: [
                  Text(
                    'minutes',
                    style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(207, 224, 233, 10)),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.all(17)),
              Column(
                children: [
                  Text(
                    'seconds',
                    style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(207, 224, 233, 10)),
                  )
                ],
              ),
            ],
          ),
          Text(
            '03:23:64',
            style: TextStyle(
              fontFamily: 'Raleway',
              color: Colors.white70,
              fontWeight: FontWeight.bold,
              fontSize: 64,
              shadows: <Shadow>[
                Shadow(
        offset: Offset(0.0, 2.0),
        blurRadius: 9.0,
        color: Colors.black45
      ),
      
              ]
            ),
          )
        ],
      ),
    );
  }
}
