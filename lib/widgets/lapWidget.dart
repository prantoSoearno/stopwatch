import 'package:flutter/material.dart';

class LapWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.black12,
      margin: EdgeInsets.only(top: 10),
      height: MediaQuery.of(context).size.height * 0.22,
      child: ListView(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Text(
                    '2',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(207, 224, 233, 10)),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 30)),
              Column(
                children: [
                  Text(
                    '+01:22:32',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(207, 224, 233, 10)),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.all(15)),
              Column(
                children: [
                  Text(
                    '04:45:64',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(207, 224, 233, 10)),
                  )
                ],
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Text(
                    '3',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(207, 224, 233, 10)),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 30)),
              Column(
                children: [
                  Text(
                    '+01:22:32',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(207, 224, 233, 10)),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.all(15)),
              Column(
                children: [
                  Text(
                    '04:45:64',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(207, 224, 233, 10)),
                  )
                ],
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Text(
                    '1',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(207, 224, 233, 10)),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 30)),
              Column(
                children: [
                  Text(
                    '+01:22:32',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(207, 224, 233, 10)),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.all(15)),
              Column(
                children: [
                  Text(
                    '04:45:64',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(207, 224, 233, 10)),
                  )
                ],
              ),
            ],
          )
        ],
        scrollDirection: Axis.vertical,
      ),
    );
  }
}
