import 'package:flutter/material.dart';
import 'package:stopwatch/widgets/buttonWidget.dart';
import 'package:stopwatch/widgets/headerWidget.dart';
import 'package:stopwatch/widgets/lapWidget.dart';
import 'package:stopwatch/widgets/stopwatchWidget.dart';
import 'package:stopwatch/widgets/timeWidget.dart';

class HomeWidget extends StatefulWidget {
  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          
          decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color.fromRGBO(0, 82 , 125, 70),Color.fromRGBO(1, 167, 126, 100)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  )
              ),
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  HeaderWidget(),
                  StopwatchWidget(),
                  TimeWidget(),
                  LapWidget(),
                  ButtonWidget()
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}